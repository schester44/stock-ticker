import api from "../api"

export const TICKER_INFO_RECEIVED = "TICKER_INFO_RECEIVED"

export const tickerInfoReceived = data => ({
    type: TICKER_INFO_RECEIVED,
    data
})

export const getTickerInfo = symbols => dispatch =>
	api.ticker.getBatch(symbols).then(data => dispatch(tickerInfoReceived(data)))
