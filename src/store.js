import { createStore, applyMiddleware, compose } from "redux"
import { routerMiddleware } from "react-router-redux"
import thunk from "redux-thunk"
import createHistory from "history/createBrowserHistory"
import rootReducer from "./reducers"

export const history = createHistory()

const initialState = {}
const enhancers = []
const middleware = [routerMiddleware(history), thunk]

if (process.env.NODE_ENV === "development") {
	if (typeof window.devToolsExtension === "function") {
		enhancers.push(window.devToolsExtension())
	}
}

const composedEnhancers = compose(applyMiddleware(...middleware), ...enhancers)

export default createStore(rootReducer, initialState, composedEnhancers)
