import axios from "axios"

export default {
	ticker: {
		getBatch: symbols =>
			axios
				.get(
					`https://api.iextrading.com/1.0/stock/market/batch?symbols=${symbols.join(
						","
					)}&range=1m&last=5&types=quote,news,chart`
				)
				.then(res => res.data)
	}
}
