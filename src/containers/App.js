import React from 'react';
import { Route, withRouter } from "react-router-dom"
import asyncComponent from "components/async-component"
import * as routes from "routes"

const Home = asyncComponent(() => import('./Home'))

const App = () => {
    return (
        <div style={{ width: "100%", height: "100%"}}>
            <Route exact path={routes.HOME} component={Home} />
        </div>
    );
}

export default withRouter(App);