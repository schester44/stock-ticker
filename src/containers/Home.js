import React, { Component } from "react"
import { connect } from "react-redux"
import { getTickerInfo } from "actions/ticker"
import { companyInfoSelector } from "reducers/ticker"
import styled from "styled-components"

import CompanyOverview from "components/CompanyOverview/index.js"

const Wrapper = styled.div`
width: 100%;
height: 100%;
padding: 3% 6%;
`

class Home extends Component {
	componentDidMount() {
		this.props.getTickerInfo(["appl", "fb", "goog", "amzn", "msft"]).catch(e => {
			console.log("request error", e)
		})
	}
	render() {
		return <Wrapper>
			<div style={{ height: "100%", display: 'flex' }}>
				<div style={{ width: "70%", height: "100%" }}>
					<CompanyOverview companies={this.props.companies} />
				</div>
			</div>	
		</Wrapper>
	}
}

const mapStateToProps = state => ({
	companies: companyInfoSelector(state)
})

export default connect(mapStateToProps, { getTickerInfo })(Home)
