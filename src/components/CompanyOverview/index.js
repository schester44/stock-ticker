import React from "react"
import styled from "styled-components"
import { format } from "date-fns"
import CompanyListing from "./CompanyListing"

const Wrapper = styled.div`
	padding: 20px;
	background: rgba(32, 32, 32, 0.75);
	height: 100%;
`

const Header = styled.h1`
	color: white;
	margin-bottom: 20px;
	text-transform: uppercase;
	font-size: 3vw;

	span {
		color: rgba(33, 192, 241, 1);
	}
`

const CompanyOverview = ({ companies }) => {
	return (
		<Wrapper>
			<Header>
				<span>STOCK REPORT FOR</span> {format(new Date(), "MMMM DD")}
			</Header>
			{companies.map(company => <CompanyListing key={company.symbol} company={company} />)}
		</Wrapper>
	)
}

export default CompanyOverview
