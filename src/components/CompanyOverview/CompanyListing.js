import React from "react"
import styled from "styled-components"

const Company = styled.div`
	padding: 20px;
	border-top: 3px solid rgba(33, 192, 241, 1);
	font-size: 3vw;
	font-weight: 100;
	letter-spacing: 1px;
	color: white;
	display: flex;
	justify-content: space-between;
	align-items: center;
`

const Name = styled.div`
	flex: 1;
	h2 {
		font-size: 4vw;
	}

	h4 {
		font-size: 1.5vw;
		color: #999;
	}
`
const Volume = styled.div`
    width: 25%;
`
const Change = styled.div`
	color: ${props => (props.direction === "up" ? `white` : `red`)};
    width: 20%;
    text-align: right;
`
const Direction = styled.div`
	width: 10%;
    display: flex;
    justify-content: flex-end;

	div {
		width: 50px;
		height: 50px;
		border-radius: 10px;
		border: 3px solid;
		display: flex;
		justify-content: center;
		align-items: center;
		border-color: ${props => (props.direction === "up" ? `white` : `red`)};
	}

	i {
		border: solid black;
		border-width: 0 3px 3px 0;
		display: inline-block;
		padding: 3px;
		transform: ${props => (props.direction === "up" ? `rotate(-135deg)` : `rotate(45deg)`)};
		border-color: ${props => (props.direction === "up" ? `white` : `red`)};
	}
`

const CompanyListing = ({ company }) => {
	return (
		<Company>
			<Name>
				<h2>{company.symbol}</h2>
				<h4>{company.companyName}</h4>
			</Name>
			<Volume>{company.avgTotalVolume}</Volume>
			<Change direction={company.priceChangePercentage > 0 ? "up" : "down"}>
				{" "}
				{(company.priceChangePercentage * 100).toFixed(2)}%
			</Change>
            <Direction direction={company.priceChangePercentage > 0 ? "up" : "down"}>
                <div><i></i></div>
            </Direction>    
		</Company>
	)
}

export default CompanyListing
