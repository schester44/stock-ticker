import { combineReducers } from "redux";
import { routerReducer } from "react-router-redux"
import ticker from "./ticker"

export default combineReducers({
    router: routerReducer,
    ticker
})