import { TICKER_INFO_RECEIVED } from "../actions/ticker"

export default function ticker(state = {}, action = {}) {
	switch (action.type) {
		case TICKER_INFO_RECEIVED:
			return action.data
		default:
			return state
	}
}


export const companyInfoSelector = (state) => {
	const info = []

	Object.keys(state.ticker).forEach(symb => {
		const company = state.ticker[symb]

		info.push({
			symbol: symb,
			companyName: company.quote.companyName,
			latestPrice: company.quote.latestPrice,
			priceChangePercentage: company.quote.changePercent,
			avgTotalVolume: company.quote.avgTotalVolume,
			chart: company.chart,
			news: company.news
		})
	})

	return info
}